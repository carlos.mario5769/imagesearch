package com.ecci.bingimages.data

import com.google.gson.annotations.SerializedName

data class ImageResponse(
    val name: String,
    val thumbnailUrl: String)

