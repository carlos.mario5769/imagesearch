package com.ecci.bingimages

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.ecci.bingimages.data.RetrofitInstance
import com.ecci.bingimages.data.SearchResponse
import com.ecci.bingimages.databinding.ActivityMainBinding
import com.xwray.groupie.GroupieAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.resultsRecyclerView.layoutManager = GridLayoutManager(this, 2)

        val adapter = GroupieAdapter()
        binding.resultsRecyclerView.adapter = adapter

        val activity = this

        binding.searchButton.setOnClickListener {
            val term = binding.searchEditText.text.toString()
            if(term.isEmpty()) {
                Toast.makeText(activity, "El texto no puede estar vacio", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            RetrofitInstance().bingApi().searchImages(term).enqueue(object: Callback<SearchResponse> {
                override fun onResponse(
                    call: Call<SearchResponse>,
                    response: Response<SearchResponse>
                ) {
                    response.body()?.let {
                        val listImage = it.list
                        val items = listImage.map { ImageItem(it) }
                        adapter.update(items)
                    }
                }

                override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
                    Toast.makeText(activity, "El servicio ha fallado", Toast.LENGTH_SHORT).show()
                }

            })
        }


    }
}