package com.ecci.bingimages

import com.ecci.bingimages.data.ImageResponse
import com.ecci.bingimages.databinding.ItemImageBinding
import com.squareup.picasso.Picasso
import com.xwray.groupie.databinding.BindableItem

class ImageItem(private val image: ImageResponse): BindableItem<ItemImageBinding>() {
    override fun bind(viewBinding: ItemImageBinding, position: Int) {
        viewBinding.titleTextView.text = image.name
        //Cargamos una imagen desde una url a el ImageView
        Picasso.get().load(image.thumbnailUrl).into(viewBinding.imageView)
    }

    override fun getLayout(): Int {
        return R.layout.item_image
    }
}