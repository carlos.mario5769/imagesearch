package com.ecci.bingimages.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface BingApi {

    @Headers("x-rapidapi-key: 8a6e281627mshf918ee517db9717p150194jsn325953bfdff0")
    @GET("images/search")
    fun searchImages(@Query("q") query: String): Call<SearchResponse>
}