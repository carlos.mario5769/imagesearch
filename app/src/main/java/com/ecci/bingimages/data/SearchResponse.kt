package com.ecci.bingimages.data

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("value")
    val list: List<ImageResponse>,
    val totalEstimatedMatches: Int
)