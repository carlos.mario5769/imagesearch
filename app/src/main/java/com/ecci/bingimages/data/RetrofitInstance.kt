package com.ecci.bingimages.data

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInstance {
    private fun getInstance(): Retrofit {
        val gson = GsonBuilder().create()
        return Retrofit.Builder()
            .baseUrl("https://bing-image-search1.p.rapidapi.com/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    fun bingApi(): BingApi {
        return getInstance().create(BingApi::class.java)
    }
}